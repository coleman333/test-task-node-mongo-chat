var socket = io('localhost:3023');
var loginForm = document.querySelector('#login-form');

loginForm.addEventListener('submit', function (e) {
  e.preventDefault();
  var input = document.querySelector('#password');
  var password = input.value;
  var emailInput = document.querySelector('#email');
  var email = emailInput.value;
  socket.emit('login', { password, email });
});

socket.on('login', function (accessToken) {
  console.log('logged in', accessToken)
  window.accessToken = accessToken;
});

var form = document.querySelector('form');

form.addEventListener('submit', function (e) {
  e.preventDefault();
  var input = document.querySelector('#message');
  var text = input.value;
  // var emailInput = document.querySelector('#email');
  // var email = emailInput.value;
  socket.emit('message', { text, accessToken: window.accessToken });
  input.value = '';
});

socket.emit('getMessages', window.accessToken);
socket.on('messages', function (messages = []) {
  messages.forEach(onMessage);
});

function onMessage(message) {
  if (!message) {
    return;
  }
  var container = document.querySelector('section');
  var newMessage = document.createElement('p');
  newMessage.innerText = `${message.email}: ${message.text} \n 
  ${message.createdAt}`;
  container.appendChild(newMessage);

  var seperator = document.createElement('br');
  container.appendChild(seperator);

  container.scrollTop = container.scrollHeight;
}

socket.on('message', onMessage);


function connect() {
  axios.get('/api/longpooling')
    .then((response) => {
      console.log('response', response.data.message);
      connect();
    })
    .catch(err => console.error(err));
}

function send2() {
  var input = document.querySelector('#longpooling-input');
  var message = input.value;
  axios.post('/api/longpooling', { message }).catch(err => console.error(err));
}