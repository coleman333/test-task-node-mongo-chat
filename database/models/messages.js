const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const messages = new Schema({
  text: { type: String, required: true },
  email: { type: String, required: true },
  author: { type: String, required: true },
  createdAt: { type: Date, required: false, default: Date.now },
  updatedAt: { type: Date, required: false, default: Date.now },
});

module.exports = mongoose.model('massages', messages);

