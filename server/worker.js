const http = require('http');
const https = require('https');
const fs = require('fs');
const path = require('path');
const cluster = require('cluster');
const app = require('./config/express');
const logger = require('./config/logger');
const debug = require('debug')('project-name:server');

const userModel = require('../database/models/userModel');
const authCtrl = require('./controllers/Auth')({ userModel });

// TODO: create HTTPS server
// Workers can share any TCP connection
// In this case it is an HTTP server
let server;
if (process.env.NODE_ENV === 'production') {
  server = https.createServer({
    key: fs.readFileSync(path.join(__dirname, '/ssl/key.pem')),
    cert: fs.readFileSync(path.join(__dirname, '/ssl/cert.pem')),
    passphrase: 'secret'
  }, app).listen(app.get('port'));
} else {
  server = http.createServer(app).listen(app.get('port'));
}

server.on('error', onError);
server.on('listening', onListening.bind(null, server));

const io = require('socket.io')(server);

io.on('connection', function (socket) {
  // socket.on('message', async function (data) {
  //   try {
  //     const accessToken = data.accessToken;
  //     const user = await authenticateUserByToken(accessToken);
  //     data.email = user.email;
  //     const newMessage = await writeMessageIntoBase(data);
  //     io.emit('message', newMessage);
  //   } catch (e) {
  //     return socket.emit('message', { text: e.message });
  //   }
  //
  // });

  // socket.on('getMessages', async function (accessToken) {
  //   try {
  //     await authenticateUserByToken(accessToken);
  //     const messages = await getMessages();
  //     socket.emit('messages', messages);
  //   } catch (e) {
  //     return socket.emit('message', { text: e.message });
  //   }
  // });

  socket.on('login', async function (auth) { // auth = {email,password}
    const req = {
      body: auth,
    };
    const res = {
      json({ accessToken }) { // {accessToken}
        socket.emit('login', accessToken);
      }
    };
    const next = async function (err) {
      socket.emit('custom-error', err.message);
    };

    authCtrl.login(req, res, next);
  });

  socket.on('logout', async function ( accessToken ) {
    const req = {
      query: accessToken,
    };

    const res = {
      send( {message} ) {
        socket.emit('logout',message);
      }
    };
    const next = async function (err) {
      socket.emit('custom-error', err.message);
    };

    authCtrl.logout(req, res, next);
  });

});

function onError(error) {
  if (!['listen', 'bind'].includes(error.syscall)) {
    throw error;
  }
  const port = app.get('port');
  let bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES': {
      logger.error(bind + ' requires elevated privileges');
      if (cluster.isWorker) {
        cluster.worker.send({ type: 'shutdown', code: 1 });
      }
    }
      break;
    case 'EADDRINUSE': {
      logger.error(bind + ' is already in use');
      if (cluster.isWorker) {
        cluster.worker.send({ type: 'shutdown', code: 1 });
      }
    }
      break;
    default:
      throw error;
  }
}

function onListening(server) {
  let addr = server.address();
  let bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}
