const express = require('express');
const router = express.Router();
const userModel = require('../../database/models/userModel');
const authCtrl = require('../controllers/Auth')({ userModel });

router.post('/login', authCtrl.login);
// router.get('/logout', authCtrl.logout);
router.post('/registration', authCtrl.registration);

module.exports = router;
