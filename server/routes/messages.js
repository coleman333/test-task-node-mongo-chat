const express = require('express');
const router = express.Router();
// const userCtrl = require('../controllers/users');
const messageCtrl = require('../controllers/message');

// router.post('/',userCtrl.createUser);                          // create new user
router.get('/list/:page',messageCtrl.getAllMessages);             // get all messages
router.get('/single/:message_id',messageCtrl.getMessageById);     // get  message by id
router.post('/',messageCtrl.createMessage);                       // create new message

module.exports = router;


