const messages = require('./messages');
const auth = require('./auth');
const swaggerUi = require('swagger-ui-express');
// const yamlFile = require('../../swagger/routes.yaml');
const YAML = require('yamljs');
const swaggerDocumentAccount = YAML.load(require('path').join(__dirname, '../../swagger/routes.yaml'));

const connections = [];

module.exports = (app) => {
  app.use('/api/messages/', messages);
  app.use('/api/auth/', auth);
  app.use('/doc/', swaggerUi.serve, swaggerUi.setup(swaggerDocumentAccount));

  app.get('/api/longpooling', (req, res, next) => {
    req.setTimeout(0);
    connections.push(res);
  });

  app.post('/api/longpooling', (req, res, next) => {
    const { message } = req.body;
    res.end();
    // connections [2,3,1,4,5]
    let response = connections.pop(); // response = 5 //   [2,3,1,4]
    while (response) {
      response.json({ message: message });
      response = connections.pop(); // response = null //   []
    }
  });

  app.use((err, req, res, next) => {
    console.error(err);
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: app.get('env') === 'development' ? err : {}
    });
  });
};
