class Controller {

  constructor(db = {}) {
    this._db = db;
    const func = this.action;

    this.action = async (req, res, next) => {
      try {
        await this.validate(req, res, next);
        await func.call(this, req, res, next);
      } catch ( ex ) {
        next(ex);
      }
    };

  }

  /**
   * abstract
   * @param {Object} req - description
   * @param {Object} res - description
   * @param {Function} next - description
   * @returns {Promise<void>} - description
   */
  async validate(req, res, next) {
    // joi
  }

  /**
   * abstract
   * @param {Object} req - description
   * @param {Object} res - description
   * @param {Function} next - description
   * @returns {Promise<void>} - description
   */
  async action(req, res, next) {
    throw new Error('Method \'action\' not implemented.');
  }

}

module.exports = Controller;
