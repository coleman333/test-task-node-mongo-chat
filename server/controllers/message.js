let messagesModel = require('../../database/models/messages');
const _ = require('lodash');

const fs = require('fs');


module.exports.getAllMessages = async function (req, res, next) {
  try {
    const { page } = req.params;

    let limit=10;
    let offset = page*limit;
    const messages = await messagesModel.find({})
      .limit(limit)
      .skip(offset);

    res.status(200).send(messages);

  } catch (e) {
    next(e)
  }
};

module.exports.getMessageById = async function (req, res, next) {
  try {
    const { message_id } = req.params;
    const message = await messagesModel.findById(message_id)
    res.status(200).send(message);

  } catch (e) {
    next(e)
  }
};


module.exports.createMessage = async function (req, res, next) {
  try {
    const { email, text, author } = req.body;

    if (!email || !author || !text) {
      return res.status(404).send({ message: 'You need to pass all params' })
    }
    if( text === '' || text.length >= 100){
      return res.status(404).send({ message: 'message has incorrect format' })
    }
    const isValidEmail = await validateEmail(email);

    if (!isValidEmail) {
      return res.status(404).send({ message: 'The email is incorrect' })
    }

    const message = await messagesModel.create({email, text, author});

    res.status(200).send({ message: 'Success', item: message })
  } catch (e) {
    next(e)
  }
};


function validateEmail(email) {
  const res = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return res.test(email);
}

