const Controller = require('../Controller');

class Logout extends Controller {

  async action(req, res, next) {
    try {
      const { accessToken } = req.query;
      await this._db.userModel.update({ accessToken },{$unset: {accessToken: 1 }});
      res.send('success');
    } catch (ex) {
      next(ex);
    }
  }

}

module.exports = Logout;
