const Controller = require('../Controller');

class Registration extends Controller {

  async action(req, res, next) {
    try {
      const { email, password } = req.body;

      let user = await this._db.userModel.findOne({ email });
      if (user) {
        throw new Error('User already exists.');
      }

      user = await this._db.userModel.create({ email, password });

      res.json({ user });
    } catch (ex) {
      next(ex);
    }
  }

}

module.exports = Registration;
