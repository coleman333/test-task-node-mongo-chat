const Controller = require('../Controller');
// const Joi = require('joi');
const util = require('util');
const _ = require('lodash');
const uuidv1 = require('uuid/v1');

class Login extends Controller {

  async action(req, res, next) {
    try {
      const { email, password } = req.body;

      const user = await this._db.userModel.findOne({ email });
      if (!user) {
        throw new Error('User not found.');
      }

      const isMatch = await util.promisify(user.comparePassword).call(user, _.toString(password));

      if (!isMatch) {
        throw new Error('Email or password doesn\'t match.');
      }

      const accessToken = uuidv1();

      await this._db.userModel.update({ email }, { accessToken });

      res.json({ accessToken });
    } catch (ex) {
      next(ex);
    }
  }

  async validate(req, res, next) {
    // const bodySchema = Joi.object().keys({
    //   email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    //   password: Joi.string().required(),
    //   remember: Joi.boolean(),
    //   newPassword: Joi.string().regex(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_])\S{6,}$/),
    //   verificationCode: Joi.string(),
    // });
    //
    // return Joi.validate(req.body, bodySchema);
  }
}

module.exports = Login;
