const Controller = require('../Controller');

class IsAuthenticated extends Controller {

  async action(req, res, next) {
    try {
      const { accessToken } = req.query;

      const user = await this._db.userModel.findOne({ accessToken });
      if (!user) {
        throw new Error('Access denied.');
      }

      next();
    } catch ( ex ) {
      next(ex);
    }
  }

}

module.exports = IsAuthenticated;
